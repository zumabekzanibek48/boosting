import pandas as pd

# Импорттауды орындаймыз
data = pd.read_csv('C:\\Users\\Пользователь\\OneDrive\\Рабочий стол\\3 course\\ИИ\\data.csv') 

# Еркектерге арналған деректерді фильтрлейміз
data_male = data[data['Пол'] == 'М']

# Біз деректерді автомобиль маркасы бойынша топтастырамыз және жазбалар санын есептейміз
popular_car_brand = data_male['Марка'].value_counts().idxmax()
car_brand_count = data_male['Марка'].value_counts().max()

# Шешімді шығырамыз
print(f"Ерлер арасында ең танымал автомобиль бренді: {popular_car_brand}, Жазба саны: {car_brand_count}")