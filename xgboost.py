import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import AdaBoostClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score

# Берілген деректерден data Frame жасадым
data = pd.read_csv('C:\\Users\\Пользователь\\OneDrive\\Рабочий стол\\3 course\\ИИ\\data.csv')  

label_encoder = LabelEncoder()
data['Марка'] = label_encoder.fit_transform(data['Марка'])
data['Город'] = label_encoder.fit_transform(data['Город'])
data['Пол'] = label_encoder.fit_transform(data['Пол'])

# Деректерді (X) және мақсатты айнымалыға (y)бөлдім
X = data[['Город', 'Пол']]
y = data['Марка']

# Деректерді оқу және тест жиынтықтарына бөлдім
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Adaboost моделін инициализациялау және оқыту
model = AdaBoostClassifier(n_estimators=100, random_state=42)
model.fit(X_train, y_train)

# Біз тест жиынтығында болжам жасаймыз
y_pred = model.predict(X_test)

# Модельдің дәлдігін есептейміз
accuracy = accuracy_score(y_test, y_pred)
a = accuracy*100
print(f" AdaBoost моделінің ауытқуы: {a}" + "%")