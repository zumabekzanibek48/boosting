import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import accuracy_score

rcParams['font.family'] = 'DejaVu Sans'

df = pd.read_csv('data.csv')

dark_colors = ['черный', 'темно-синий', 'серый', 'темно-вишневый', 'темно-синий металлик', 'темно-синий металлик архивный']
light_colors = ['белый', 'серебристый', 'серебристый металлик архивный', 'светло-зелёный', 'бежевый', 'светло-серый', 'светло-серый металлик', 'светло-зеленый', 'белый', 'серый металлик', 'серебристый металлик', 'серебристый металлик архивный', 'серый']

df['Color_Category'] = df['Цвет'].apply(lambda x: 'Темные' if isinstance(x, str) and x.lower() in dark_colors else 'Светлые')

grouped_df = df.groupby('Color_Category').size().reset_index(name='Counts')

colors = {'Темные': 'darkgray', 'Светлые': 'lightgray'}

plt.bar(grouped_df['Color_Category'], grouped_df['Counts'], color=[colors[x] for x in grouped_df['Color_Category']])
plt.xlabel('Түстер категориясы')
plt.ylabel('Автомобильдер санны')
plt.title('Автомобиль түстерінің квази сызықты құрамы')
plt.show()

# Оқу үшін белгілерді таңдау
features = ['Возраст полных лет', 'Объём двигателя', 'Год выпуска']
target = 'Цвет'

# Категориялық белгілерді сандық белгілермен ауыстыру
df = pd.get_dummies(df, columns=['Пол'])

df['Год выпуска'] = pd.to_datetime(df['Год выпуска']).dt.year

# Өткізіп алған мәндерді жою
df = df.dropna(subset=[target] + features)

X_train, X_test, y_train, y_test = train_test_split(df[features], df[target], test_size=0.2, random_state=42)

rf = RandomForestClassifier(random_state=42)
lr = LogisticRegression(random_state=42)

rf.fit(X_train, y_train)
lr.fit(X_train, y_train)

# Модельдерді араластыру
ensemble = VotingClassifier(estimators=[('rf', rf), ('lr', lr)], voting='hard')
ensemble.fit(X_train, y_train)

# Дәлділікті анықтау
y_pred = ensemble.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f'Дәлділігі: {accuracy}')
